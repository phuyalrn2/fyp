//express framework for server
const express = require("express");
const app = express();
//development dependency to check route
const morgan = require("morgan");
app.use(morgan("dev"));
// package for mongodb database
const mongoose = require("mongoose");
// to initilize session
const session = require("express-session");
// convert form data to json
const bodyParser = require("body-parser");
// for os path
const path = require("path");
// mongodb storage for session
const MongoDbStore = require("connect-mongodb-session")(session);
// environment variable
const dotenv = require("dotenv");
// User model
const User = require("./model/User");

// csrf middleware
const csrf = require("csurf");
const csrfProtection = csrf();
const csrfMiddleware = require("./middleware/csufr");

// initilize dont env
dotenv.config();
database = process.env.DATABASE;
port = process.env.PORT || 3000;
session_secret = process.env.SESSION;

// initilize mongodb store for session
const store = new MongoDbStore({
  uri: database,
  collection: "session"
});

// set view engine
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(express.static(path.join(__dirname, "public")));
// body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

// // Session Middle ware
app.use(
  session({
    secret: session_secret,
    resave: false,
    saveUninitialized: true,
    store: store
  })
);

// Check user loggin status
app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }
  User.findById(req.session.user._id)
    .then(user => {
      req.user = user;
      next();
    })
    .catch(err => console.log(err));
});

app.use((req, res, next) => {
  res.locals.isAuthenticated = req.session.isLoggedIn;
  res.locals.userType = req.session.userType;
  next();
});

mongoose
  .connect(database, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(data => {
    console.log("connected to database");
    app.listen(port, () => {
      console.log(`listening to the port ${port}`);
    });
  })
  .catch(err => console.log(err));
// routes
//
// user route
const jwt = require("jsonwebtoken");
const userauth = require("./route/userAuth");
app.use("/api/auth", userauth);
const jwtToken = require("./middleware/jwtToken");
const user = require("./route/user");
app.use("/api", jwtToken, user);
// admin routes
// admin middleware
const adminMiddleware = require("./middleware/isAdmin");
const adminRoute = require("./route/admin");
app.use("/admin", csrfProtection, csrfMiddleware, adminMiddleware, adminRoute);

// public route
const public = require("./route/public");
app.use(public);

// route  to auth
const auth = require("./route/auth");
app.use(csrfProtection, csrfMiddleware, auth);
