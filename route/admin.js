const express = require("express");
const router = express.Router();
const adminMiddleware = require("../middleware/isAdmin");
const {
  getUsers,
  getEditUser,
  getDeleteUser,
  postAddRoom,
  getDeleteRoom,
  postAddDevice,
  postDeleteDevice,
  getRooms,
  getDevices
} = require("../controller/admin");

const {
  roomNameValidation,
  deviceNameValidation
} = require("../validation/adminValidation");

router.get("/", adminMiddleware, getUsers);
router.get("/user/:userId", adminMiddleware, getRooms);
router.get("/edit-user/:userId", adminMiddleware, getEditUser);
router.get("/delete-user/:userId", adminMiddleware, getDeleteUser);
router.post("/add-room", adminMiddleware, roomNameValidation, postAddRoom);

router.post(
  "/add-device",
  adminMiddleware,
  deviceNameValidation,
  postAddDevice
);

router.post("/delete-room", adminMiddleware, getDeleteRoom);

router.get(
  "/delete-device/:roomId/:deviceId",
  adminMiddleware,
  postDeleteDevice
);

router.get("/user/device/:roomId", adminMiddleware, getDevices);

module.exports = router;
