const router = require("express").Router();
const {
  getIndex,
  getRooms,
  getDevices,
  postChangeDeviceStatus
} = require("../controller/user");

router.get("/", getIndex);
router.get("/rooms", getRooms);
router.get("/devices", getDevices);
router.post("/deviceStatus", postChangeDeviceStatus);
module.exports = router;
