const express = require("express");
const publicMiddleware = require("../middleware/publicMiddleware");

const router = express.Router();
const { getIndex } = require("../controller/public");

router.get("/", publicMiddleware, getIndex);

module.exports = router;
