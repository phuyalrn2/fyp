const express = require("express");
const router = express.Router();
const { postLogin } = require("../controller/userAuth");
router.post("/login", postLogin);
module.exports = router;
