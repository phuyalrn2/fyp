// middleware to check if user is logged in
const authenticatedMiddleware = require("../middleware/isLoggedIn");
// Validation files
const {
  firstName_validation,
  lastName_validation,
  emailValidation,
  passwordValidation,
  phoneNoValidation,
  genderValidation
} = require("../validation/authValidation");
// express router
const express = require("express");
const router = express.Router();
// controllers
const {
  getSignup,
  postSignup,
  getLogin,
  postLogin,
  postLogout
} = require("../controller/auth");
// signup route
router.get("/signup", getSignup);
router.post(
  "/signup",
  firstName_validation,
  lastName_validation,
  emailValidation,
  passwordValidation,
  phoneNoValidation,
  genderValidation,
  postSignup
);

router.get("/login", authenticatedMiddleware, getLogin);
router.post("/login", authenticatedMiddleware, postLogin);
router.get("/logout", postLogout);

module.exports = router;
