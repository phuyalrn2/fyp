const Room = require("../model/Rooms");
exports.findRoomById = (userId, cb) =>
  Room.find({ user: userId })
    .then(rooms => {
      return cb(rooms);
    })
    .catch(err => {
      throw err;
    });

exports.findRoom = (userId, name, cb) => {
  Room.find({ user: userId, name })
    .then(rooms => {
      return cb(rooms);
    })
    .catch(err => {
      throw err;
    });
};
