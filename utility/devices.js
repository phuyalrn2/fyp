const Room = require("../model/Rooms");
exports.findDevice = (roomId, cb) => {
  Room.findById(roomId)
    .then(room => {
      cb(room);
    })
    .catch(err => {
      throw err;
    });
};

// exports.deviceStatus = (roomId, deviceId, cb) => {
//   Room.findById(roomId)
//     .then(room => {
//       if (room.devices > 0) {
//         let i = 0;
//         for (i; i < room.devices.length; i++) {
//           if (room.devices[i]._id == deviceId) {
//             return cb(room.devices[i]);
//           }
//         }
//         if (i == rooms.devices.length - 1) {
//           return resizeBy.json({
//             err: "Room with this id dosent found"
//           })
//         }
//       } esle{
//         return "dsa"
//       }
//     })
//     .catch(err => {
//       throw err;
//     });
// };

exports.findOneDevice = (room, deviceId, cb) => {
  let i = 0;
  for (i; i < room.devices.length; i++) {
    if (room.devices[i]._id == deviceId) {
      return cb(room.devices[i]);
    }
  }
  if (i == room.devices.length) {
    throw new Error("device not found");
  }
};
