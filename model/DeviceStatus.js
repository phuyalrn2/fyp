const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const deviceStatusSchema = new Schema({
  deviceId: mongoose.Types.deviceId,
  daily: Number,
  weekly: Number,
  monthly: Number
});
