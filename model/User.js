// Model file for user
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// initilize Schema for database
const UserSchema = new Schema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  dateOfBirth: {
    type: Date
  },
  phoneNumber: {
    type: String
  },
  gender: {
    type: String
  },
  userType: {
    type: String,
    default: "normal"
  }
});
// export model
module.exports = mongoose.model("User", UserSchema);
