const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const RoomSchema = new Schema({
  name: String,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  devices: [
    {
      name: String,
      status: {
        type: Boolean,
        default: false,
        lastChangeTime: {
          type: Date,
          default: Date.now
        }
      }
    }
  ]
});
module.exports = mongoose.model("Room", RoomSchema);
