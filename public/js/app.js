$(document).ready(function($) {
  // Variables declarations
  var $wrapper = $(".main-wrapper");
  var $pageWrapper = $(".page-wrapper");
  var $slimScrolls = $(".slimscroll");
  var $sidebarOverlay = $(".sidebar-overlay");

  // Sidebar
  var Sidemenu = function() {
    this.$menuItem = $("#sidebar-menu a");
  };

  function init() {
    var $this = Sidemenu;
    $("#sidebar-menu a").on("click", function(e) {
      if (
        $(this)
          .parent()
          .hasClass("submenu")
      ) {
        e.preventDefault();
      }
      if (!$(this).hasClass("subdrop")) {
        $("ul", $(this).parents("ul:first")).slideUp(350);
        $("a", $(this).parents("ul:first")).removeClass("subdrop");
        $(this)
          .next("ul")
          .slideDown(350);
        $(this).addClass("subdrop");
      } else if ($(this).hasClass("subdrop")) {
        $(this).removeClass("subdrop");
        $(this)
          .next("ul")
          .slideUp(350);
      }
    });
    $("#sidebar-menu ul li.submenu a.active")
      .parents("li:last")
      .children("a:first")
      .addClass("active")
      .trigger("click");
  }
  // Sidebar Initiate
  init();

  // Sidebar overlay
  function sidebar_overlay($target) {
    if ($target.length) {
      $target.toggleClass("opened");
      $sidebarOverlay.toggleClass("opened");
      $("html").toggleClass("menu-opened");
      $sidebarOverlay.attr("data-reff", "#" + $target[0].id);
    }
  }

  // Mobile menu sidebar overlay
  $(document).on("click", "#mobile_btn", function() {
    var $target = $($(this).attr("href"));
    sidebar_overlay($target);
    $wrapper.toggleClass("slide-nav");
    $("#chat_sidebar").removeClass("opened");
    return false;
  });

  // Sidebar overlay reset
  $sidebarOverlay.on("click", function() {
    var $target = $($(this).attr("data-reff"));
    if ($target.length) {
      $target.removeClass("opened");
      $("html").removeClass("menu-opened");
      $(this).removeClass("opened");
      $wrapper.removeClass("slide-nav");
    }
    return false;
  });

  // Select 2
  if ($(".select").length > 0) {
    $(".select").select2({
      minimumResultsForSearch: -1,
      width: "100%"
    });
  }

  // Right Sidebar Scroll

  // Page wrapper height
  var pHeight = $(window).height();
  $pageWrapper.css("min-height", pHeight);
  $(window).resize(function() {
    var prHeight = $(window).height();
    $pageWrapper.css("min-height", prHeight);
  });

  // Datetimepicker
  if ($(".datetimepicker").length > 0) {
    $(".datetimepicker").datetimepicker({
      format: "DD/MM/YYYY"
    });
  }

  // Datatable
  if ($(".datatable").length > 0) {
    $(".datatable").DataTable({
      bFilter: false
    });
  }

  // Bootstrap Tooltip
  if ($('[data-toggle="tooltip"]').length > 0) {
    $('[data-toggle="tooltip"]').tooltip();
  }

  // Lightgallery
  if ($("#lightgallery").length > 0) {
    $("#lightgallery").lightGallery({
      thumbnail: true,
      selector: "a"
    });
  }

  // Summernote
  if ($(".summernote").length > 0) {
    $(".summernote").summernote({
      height: 200,
      minHeight: null,
      maxHeight: null,
      focus: false
    });
  }

  // Dropfiles
  if ($("#drop-zone").length > 0) {
    var dropZone = document.getElementById("drop-zone");
    var uploadForm = document.getElementById("js-upload-form");
    var startUpload = function(files) {
      console.log(files);
    };
    uploadForm.addEventListener("submit", function(e) {
      var uploadFiles = document.getElementById("js-upload-files").files;
      e.preventDefault();
      startUpload(uploadFiles);
    });
    dropZone.ondrop = function(e) {
      e.preventDefault();
      this.className = "upload-drop-zone";
      startUpload(e.dataTransfer.files);
    };
    dropZone.ondragover = function() {
      this.className = "upload-drop-zone drop";
      return false;
    };
    dropZone.ondragleave = function() {
      this.className = "upload-drop-zone";
      return false;
    };
  }

  // Small Sidebar
  if (screen.width >= 992) {
    $(document).on("click", "#toggle_btn", function() {
      if ($("body").hasClass("mini-sidebar")) {
        $("body").removeClass("mini-sidebar");
        $(".subdrop + ul").slideDown();
      } else {
        $("body").addClass("mini-sidebar");
        $(".subdrop + ul").slideUp();
      }
      return false;
    });
    $(document).on("mouseover", function(e) {
      e.stopPropagation();
      if (
        $("body").hasClass("mini-sidebar") &&
        $("#toggle_btn").is(":visible")
      ) {
        var targ = $(e.target).closest(".sidebar").length;
        if (targ) {
          $("body").addClass("expand-menu");
          $(".subdrop + ul").slideDown();
        } else {
          $("body").removeClass("expand-menu");
          $(".subdrop + ul").slideUp();
        }
        return false;
      }
    });
  }
});
