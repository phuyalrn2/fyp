module.exports = (req, res, next) => {
  if (req.session.userType === "admin") {
    return next();
  }
  return res.redirect("/");
};
