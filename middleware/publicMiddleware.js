module.exports = (req, res, next) => {
  if (req.session.userType == "admin") {
    return res.redirect("/admin");
  }
  if (req.session.userType == "user") {
    return res.redirect("/user");
  }
  next();
};
