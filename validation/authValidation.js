const { check, body } = require("express-validator");
const User = require("../model/User");

exports.firstName_validation = [
  body("firstName")
    .isString()
    .trim()
    .not()
    .isEmpty()
    .withMessage("Dont leave First Name field empty")
    .isString()
    .withMessage("FirstName Must Be String")
    .isLength({ min: 2, max: 10 })
    .withMessage("Name should between 2 and 10")
];

exports.lastName_validation = [
  body("lastName")
    .isString()
    .trim()
    .not()
    .isEmpty()
    .withMessage("Dont leave Last Name Fiend Empty")
    .isString()
    .withMessage("First Name Must Be String")
    .isLength({ min: 2, max: 10 })
    .withMessage("Last Name should between 2 and 10")
];

exports.emailValidation = [
  body("email")
    .normalizeEmail()
    .isEmail()
    .withMessage("Emails should be in someone@something.com")
    .custom((value, { req }) => {
      return User.findOne({ email: value }).then(userDoc => {
        if (userDoc) {
          return Promise.reject(
            "E-Mail exists already, please pick a different one."
          );
        }
      });
    })
];

exports.passwordValidation = [
  body("password")
    .isLength({
      min: 5,
      max: 15
    })
    .withMessage("Passsword Must be of Len gth between 5 and 15"),

  body("confirmPassword").custom((value, { req }) => {
    if (value !== req.body.password) {
      throw new Error("Passwords have to match!");
    }
    return true;
  })
];

exports.phoneNoValidation = [
  body("phoneNo")
    .isMobilePhone()
    .withMessage("Is not a Phone No")
];
exports.genderValidation = [
  body("gender")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Must Select any one gender")
];
