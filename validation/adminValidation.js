const { body } = require("express-validator");

exports.roomNameValidation = [
  body("roomName")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Room Name should be given")
];

exports.deviceNameValidation = [
  body("deviceName")
    .trim()
    .not()
    .isEmpty()
    .withMessage("Device Name should be given")
];
