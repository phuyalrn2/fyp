exports.getIndex = (req, res) => {
  res.render("index", {
    pageTitle: "Smart Energy",
    path: "/"
  });
};
