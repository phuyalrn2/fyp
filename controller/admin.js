const User = require("../model/User");
const { validationResult } = require("express-validator");
const Room = require("../model/Rooms");
const { findRoomById, findRoom } = require("../utility/findRoom");
const { findDevice } = require("../utility/devices");

//
// Get Users and display to /admin/user-list
//
exports.getUsers = (req, res) => {
  return User.find()
    .then(users => {
      return res.render("admin/user-list", {
        users,
        pageTitle: "Users",
        path: "/admin/user-list"
      });
    })
    .catch(err => console.log(err));
};

//
// Edit existing user
//
exports.getEditUser = (req, res) => {};

//
// Delete user
//
exports.getDeleteUser = (req, res) => {};

//
// Get room of selected user
//
exports.getRooms = (req, res) => {
  error = req.session.error;
  req.session.error = "";
  let errors = validationResult(req);
  errors.array().push({ msg: error });
  //errors.push({ msg: error });
  if (errors.isEmpty) {
    const userId = req.params.userId;
    User.findById(userId)
      .then(user => {
        if (user) {
          findRoomById(userId, rooms => {
            return res.render("admin/user", {
              pageTitle: user.firstName,
              rooms: rooms,
              path: "/admin/user",
              userName: user.firstName,
              id: userId
            });
          });
        } else {
          return console.log("User not found");
        }
      })
      .catch(err => {
        console.log(err);
      });
  } else {
    res.redirect(`admin/user/?error=${errors[0]}`);
  }
};
//
// Add room
//
exports.postAddRoom = (req, res) => {
  const id = req.body.id;
  const roomName = req.body.roomName;
  try {
    findRoom(id, roomName, room => {
      if (room.length == 0) {
        newRoom = new Room({
          user: id,
          name: roomName,
          devices: []
        });
        console.log(newRoom);
        return newRoom.save();
      } else {
        req.session.error = "Device already added";
        res.redirect("/admin/user/" + id);
      }
    });
  } catch (err) {
    console.log(err);
  }
  return res.redirect("/admin/user/" + id);
};

//
// Delete Room of selected user
//

exports.getDeleteRoom = (req, res) => {
  const roomId = req.params.Id;
  try {
    findRoomById(roomId, room => {
      if (room) {
        return Room.deleteOne(roomId);
      }
      console.log("Rooms Not Found");
    });
  } catch (err) {
    console.log(err);
  }
  res.redirect("/");
};

//
// Add a new device of user
//
exports.postAddDevice = (req, res) => {
  const errors = validationResult(req);
  if (errors.isEmpty) {
    const id = req.body.id;
    const deviceName = req.body.deviceName;
    Room.findById(id)
      .then(room => {
        if (room) {
          newDevices = [...room.devices];
          let i = 0;
          for (i; i < newDevices.length; i++) {
            if (deviceName === newDevices[i].name) {
              break;
            }
          }
          if (newDevices.length === i) {
            newDevices.push({ name: deviceName });
            room.devices = [...newDevices];
            return room.save();
          }
        } else {
          return console.log("Room dosen't exists");
        }
      })
      .then(room => {
        if (room) {
          res.redirect("/admin/user/device/" + room._id);
        } else {
          req.session.error = "Device already exists";
          return res.redirect("/admin/user/device/" + id);
        }
      })
      .catch(err => console.log(err + "-------------------------------"));
  } else {
    res.redirect(`admin/user/?error=${errors[0]}`);
  }
};

//
// Delete device if exists
//
exports.postDeleteDevice = (req, res) => {
  const roomId = req.params.roomId;
  const deviceId = req.params.deviceId;
  Room.findById(roomId)
    .then(room => {
      if (room) {
        newDevices = [...room.devices];
        let i = 0;
        for (i; i < newDevices.length; i++) {
          if (deviceId == newDevices[i]._id) {
            newDevices.splice(i, 1);
            break;
          }
        }
        if (newDevices.length === i) {
          req.session.error = "device not found";
        } else {
          room.devices = [...newDevices];
          return room.save();
        }
      }
    })
    .then(result => {
      return res.redirect("/admin/user/device/" + roomId);
    })
    .catch(err => console.log(err));
};

exports.getDevices = (req, res) => {
  roomId = req.params.roomId;
  error = req.session.error ? req.session.error : "";
  req.session.error = "";
  try {
    findDevice(roomId, room => {
      if (room)
        return res.render("admin/room", {
          devices: room.devices,
          pageTitle: "Devices",
          path: "/admin/user",
          error: error,
          id: room._id
        });
      else console.log("Room not found");
    });
  } catch (err) {
    console.log(err);
  }
};
