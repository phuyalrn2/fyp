const Room = require("../model/Rooms");
const { findRoomById, findRoom } = require("../utility/findRoom");
const { findDevice, findOneDevice } = require("../utility/devices");

exports.getIndex = (req, res) => {
  res.json({
    message: "Welcome to api",
    user: req.user
  });
};

exports.getRooms = (req, res) => {
  try {
    userId = req.user._id;
    rooms = findRoomById(userId, rooms => res.json(rooms));
  } catch (err) {
    console.log(err);
  }
};

exports.postChangeDeviceStatus = (req, res) => {
  roomId = req.body.roomId;
  deviceId = req.body.deviceId;
  try {
    findDevice(roomId, room => {
      if (room)
        findOneDevice(room, deviceId, device => {
          device.status = device.status ? false : true;
          device.lastChangeTime = Date.now();
          room.save();
          return res.status(200).json({ suc: "success" });
        });
      else
        return res.json({
          err: "Room doesn't found"
        });
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getDevices = (req, res) => {
  roomId = req.body.roomId;

  try {
    findDevice(roomId, room => {
      if (room)
        return res.json({
          devices: room.devices
        });
      else
        return res.json({
          err: "room not found"
        });
    });
  } catch (err) {
    console.log(err);
  }
};

exports.getDevice = (req, res) => {
  roomId = req.body.roomId;

  try {
    findDevice(roomId, room => {
      if (room)
        if (room.devices.length > 0) {
        } else
          return res.json({
            err: "room not found"
          });
    });
  } catch (err) {
    console.log(err);
  }
};
