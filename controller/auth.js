const User = require("../model/User");
const { validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");

exports.getSignup = (req, res) => {
  res.render("auth/addUser.ejs", {
    pageTitle: "SignUp",
    path: "/signup",
    error: ""
  });
};

exports.postSignup = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).render("auth/addUser", {
      pageTitle: "SignUp",
      path: "/signup",
      error: errors.array()[0].msg
    });
  }
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const phoneNo = req.body.phoneNo;
  const gender = req.body.gender;
  bcrypt
    .hash(password, 12)
    .then(hashedPassword => {
      return User.find().then(data => {
        return new User({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: hashedPassword,
          phoneNumber: phoneNo,
          gender: gender,
          userType: data.length === 0 ? "admin" : "normal"
        });
      });
    })
    .then(user => {
      return user.save();
    })
    .then(user => {
      console.log("User Created");
      res.redirect("/signup");
    })
    .catch(err => console.log(err));
};

exports.getLogin = (req, res) => {
  res.render("auth/login", {
    pageTitle: "SignUp",
    path: "/login",
    error: ""
  });
};

exports.postLogin = (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  User.findOne({ email: email }).then(user => {
    if (!user) {
      return res.render("auth/login", {
        pageTitle: "SignUp",
        path: "/login",
        error: "User Not Found"
      });
    }
    bcrypt.compare(password, user.password).then(doMatch => {
      if (doMatch) {
        req.session.isLoggedIn = true;
        req.session.user = user;
        req.session.userType = user.userType;
        res.redirect("/");
      } else {
        res.render("auth/login", {
          pageTitle: "SignUp",
          path: "/login",
          error: "Email or password not found"
        });
      }
    });
  });
};

exports.postLogout = (req, res, next) => {
  req.session.destroy(err => {
    console.log(err);
    res.redirect("/");
  });
};
