const jwt = require("jsonwebtoken");
const User = require("../model/User");
const bcrypt = require("bcryptjs");
exports.postLogin = (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  User.findOne({ email: email })
    .then(user => {
      console.log(user);
      if (!user) {
        return res.status(400).json({
          error: "User or password is incorrect"
        });
      }
      return bcrypt.compare(password, user.password).then(doMatch => {
        if (doMatch) {
          jwt.sign(
            {
              _id: user._id,
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
              phoneNumber: user.phoneNumber,
              gender: user.gender
            },
            process.env.JWT,
            (err, token) => {
              res.json({
                token
              });
            }
          );
        } else {
          return res.status(400).json({
            error: "User or password is incorrect"
          });
        }
      });
    })
    .catch(err => console.log(err));
};
